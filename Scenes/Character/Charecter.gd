extends KinematicBody2D

export (int) var speed = 200

var _fireball = preload("res://Scenes/Skills/Fireball/Fireball.tscn")
var target = Vector2()
var fireballTarget = Vector2()
var velocity = Vector2()

	

func _input(event):
	if event.is_action_pressed("m_r_click"):
		target = get_global_mouse_position()
	if Input.is_action_just_pressed("ui_q"):
		if $FireballTimer.is_stopped():
			fireballTarget = get_global_mouse_position()
			fireball()

func fireball():
	var fireball = _fireball.instance()
	fireball.start($FireHand.global_position, rotation, fireballTarget)
	get_parent().add_child(fireball)
	$FireballTimer.start()

func _physics_process(delta):
	velocity = position.direction_to(target) * speed
	
	if position.distance_to(target) > 5:
		velocity = move_and_slide(velocity)
