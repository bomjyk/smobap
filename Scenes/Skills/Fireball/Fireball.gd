extends KinematicBody2D

var speed = 750
var velocity = Vector2()
var maxDistance = 300
var _target
var dist_to_target

func start(pos, dir, target):
	rotation = dir
	position = pos
	_target = target
	velocity = position.direction_to(target) * speed
	dist_to_target = position.distance_to(target)

func _physics_process(delta):
	if position.distance_to(_target) > dist_to_target - maxDistance:
		var collision = move_and_collide(velocity * delta)
		if collision:
			queue_free()
		#velocity = move_and_slide(velocity)
	else:
		queue_free()

